//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
struct point{
 float x,y;
};
float input(int j,char k)
{
 float c;
 printf(“Enter the value of %c%d “,k,j);
 scanf(“%f”,&c);
 return c;
}
float distance(struct point p1,struct point p2)
{
 float distance =sqrt (pow((p2.x - p1.x),2)+pow((p2.y - p1.y),2));
 return distance;
}
void show(float n)
{
 printf(“The distance is %f”,n);
}

int main(){
 struct point p1,p2;
 p1.x=input(1,’x’);
 p1.y=input(1,’y’);
 p2.x=input(2,’x’);
 p2.y=input(2,’y’);
 float d=distance(p1,p2);
 show(d);
 return 0;
}
