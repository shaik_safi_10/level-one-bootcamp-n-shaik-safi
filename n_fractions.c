#include<stdio.h>
typedef struct
{
    int num;
    int den;
}fraction;

int range()
{
    int n;
    printf("Enter the number of fractions:");
    scanf("%d", &n);
    return n;
}

int input(int n,fraction a[n])
{
    int i;
    for(i=0;i<n;i++)
        {
            printf("Enter numerator and denominator of the fraction %d :",i+1);
            scanf("%d %d", &a[i].num, &a[i].den);
        }
}

fraction sum(int n,fraction a[n])
{
    int numerator=0,denominator=1;
    for(int i=0;i<n;i++)
    {
        denominator = denominator * a[i].den;
    }
    for(int i=0;i<n;i++)
    {
        numerator = numerator + (a[i].num)*(denominator/a[i].den);
    }
    fraction add = {numerator,denominator};
    int i, gcd=1;
    for(i=1;i<=add.num && i<= add.den;i++)
    {
        if(add.num%i==0 && add.den%i==0)
        {
            gcd=i;
        }
    }
    add.num= add.num / gcd;
    add.den= add.den / gcd;
    return add;
}

void show(int n,fraction add)
{
    printf("Sum = %d/%d", add.num, add.den);
}

int main()
{
 int n;
 
 n=range();
 
 fraction a[n];
 
 input(n,a);
 
 fraction result=sum(n,a);
 
 show(n,result);
 
 return 0;
}